﻿using System;
using Newtonsoft.Json;

namespace wsApp
{

    public class Account
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime DOB { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account()
            {
                Name = "Joe Doe",
                Email = "joe@test.com",
                DOB = new DateTime(1976, 3, 24)
            };
            string json = JsonConvert.SerializeObject(account);
            Console.WriteLine(json);

            Console.WriteLine("Hello World!");
            Console.WriteLine("\nWhat is your name? ");
            var name = Console.ReadLine();
            var date = DateTime.Now;
            Console.WriteLine($"\nHello, {name}, on {date:d} at {date:t}!");
            Console.Write("\nPress any key to exit...");
            Console.ReadKey(true);
        }
    }
}
