using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace iot2mq
{
    public class RawData
    {
        public DateTimeOffset Date { get; set; }
        public string IMEI { get; set; }
        public string Data { get; set; }
    }

    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private IConnection connection;
        private IModel channel;
        //private IBasicProperties properties;
        private String MQ_SERVER = "localhost";
        private String MQ_USERNAME = "guest";
        private String MQ_PASSWORD = "guest";
        private String MQ_VHOST = "iot";
        private String MQ_QUEUE = "raw-data";

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;

            String connection_address = String.Format("amqp://{0}:{1}@{2}/{3}",
                         MQ_USERNAME, MQ_PASSWORD, MQ_SERVER, MQ_VHOST);


            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = new Uri(connection_address);

            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            
            channel.QueueDeclare(queue: MQ_QUEUE,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

        }

        ~Worker()
        {
            channel.Close();
            connection.Close();
            //Console.WriteLine("App closing...");
            _logger.LogInformation("Publisher closing...");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                RawData rawData = new RawData();
                rawData.Date = DateTimeOffset.Now;
                rawData.Data = "QWEDFCDXSERT&WDFGTWSDFGHY&ERTGHBVCXZSAWERTGHy67543";
                rawData.IMEI = "12345QWADSCF";
                string s = JsonSerializer.Serialize(rawData);

                byte[] bytemessage = Encoding.UTF8.GetBytes(s);

                channel.BasicPublish(
                    exchange: "",
                    routingKey: MQ_QUEUE,
                    //basicProperties: properties,
                    body: bytemessage);

                String msg = String.Format("Publisher running at: {0}", rawData.Date);
                _logger.LogInformation(msg);

                await Task.Delay(500, stoppingToken);
            }
        }





    }
}
