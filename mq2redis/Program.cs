﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using StackExchange.Redis;


namespace mq2redis
{

    public class ClientData
    {
        public string plateNumber { get; set; }
        public int companyID { get; set; }
        public int speed { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }


    class Program
    {

        static void Main(string[] args)
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
            ISubscriber sub = redis.GetSubscriber();



            String MQ_SERVER = "localhost";
            String MQ_USERNAME = "guest";
            String MQ_PASSWORD = "guest";
            String MQ_VHOST = "iot";
            String MQ_QUEUE_READ = "socket-data";


            String connection_address = String.Format("amqp://{0}:{1}@{2}/{3}",
                         MQ_USERNAME, MQ_PASSWORD, MQ_SERVER, MQ_VHOST);

            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = new Uri(connection_address);

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: MQ_QUEUE_READ,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                consumer.Received += (sender, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);


                    //
                    ClientData data = JsonSerializer.Deserialize<ClientData>(message);

                    string jsonString = JsonSerializer.Serialize(data);
                    //


                    sub.Publish(String.Format("plate-{0}", data.plateNumber), jsonString);
                    sub.Publish(String.Format("company-{0}", data.companyID), jsonString);


                    //

                    channel.BasicAck(ea.DeliveryTag, false);
                };

                channel.BasicConsume(MQ_QUEUE_READ, false, consumer);
            }
        }

    }
}
