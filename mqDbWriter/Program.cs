﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace mqDbWriter
{

    public class ParsedData
    {
        public DateTimeOffset Date { get; set; }
        public string IMEI { get; set; }
        public int speed { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }

    public class ClientData
    {
        public string plateNumber { get; set; }
        public int companyID { get; set; }
        public int speed { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }

    class Program
    {

        static void Main(string[] args)
        {
            Random rnd = new Random();
            String MQ_SERVER = "localhost";
            String MQ_USERNAME = "guest";
            String MQ_PASSWORD = "guest";
            String MQ_VHOST = "iot";
            String MQ_QUEUE_READ = "json-data";
            String MQ_QUEUE_WRITE = "socket-data";

            String connection_address = String.Format("amqp://{0}:{1}@{2}/{3}",
                         MQ_USERNAME, MQ_PASSWORD, MQ_SERVER, MQ_VHOST);

            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = new Uri(connection_address);

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: MQ_QUEUE_READ,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.QueueDeclare(queue: MQ_QUEUE_WRITE,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                consumer.Received += (sender, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);

                    //
                    ParsedData parsedData = JsonSerializer.Deserialize<ParsedData>(message);
                    ClientData extended = new ClientData();
                    extended.plateNumber = string.Format("34IOT123{0}", rnd.Next(1, 9));
                    extended.companyID = rnd.Next(1, 5);
                    extended.speed = parsedData.speed;
                    extended.lat = parsedData.lat;
                    extended.lon = parsedData.lon;

                    string jsonString = JsonSerializer.Serialize(parsedData);
                    //

                    byte[] bytemessage = Encoding.UTF8.GetBytes(jsonString);

                    channel.BasicPublish(
                        exchange: "",
                        routingKey: MQ_QUEUE_WRITE,
                        body: bytemessage);
                    //

                    channel.BasicAck(ea.DeliveryTag, false);
                };

                channel.BasicConsume(MQ_QUEUE_READ, false, consumer);
            }
        }

    }
}
