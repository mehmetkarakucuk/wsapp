﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace mqParser
{

    public class ParsedData
    {
        public DateTimeOffset Date { get; set; }
        public string IMEI { get; set; }
        public int speed { get; set; }
        public float lat { get; set; }
        public float lon { get; set; }
    }


    public class DataParser
    {
        public DataParser()
        {
        }

        public ParsedData parser(String data, String imei)
        {
            Random rnd = new Random();

            ParsedData parsedData = new ParsedData();
            parsedData.IMEI = imei;

            parsedData.speed = rnd.Next(100);
            parsedData.lat = (float)rnd.NextDouble() * 90;
            parsedData.lon = (float)rnd.NextDouble() * 90;

            return parsedData;
        }

    }
}
