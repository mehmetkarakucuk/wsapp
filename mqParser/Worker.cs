using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace mqParser
{
    public class RawData
    {
        public DateTimeOffset Date { get; set; }
        public string IMEI { get; set; }
        public string Data { get; set; }
    }

    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private IConnection connection;
        private IModel channel;
        private String MQ_SERVER = "localhost";
        private String MQ_USERNAME = "guest";
        private String MQ_PASSWORD = "guest";
        private String MQ_VHOST = "iot";
        private String MQ_QUEUE = "raw-data";
        private String MQ_QUEUE_PARSED = "json-data";

        private int counter = 0;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;

            String connection_address = String.Format("amqp://{0}:{1}@{2}/{3}",
                         MQ_USERNAME, MQ_PASSWORD, MQ_SERVER, MQ_VHOST);


            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = new Uri(connection_address);

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            channel.QueueDeclare(queue: MQ_QUEUE,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
            channel.QueueDeclare(queue: MQ_QUEUE_PARSED,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, ea) =>
            {
                counter++;
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine(" [x] Received {0}", message);

                _logger.LogInformation(message);

                //
                RawData rawData = JsonSerializer.Deserialize<RawData>(message);
                DataParser dataParser = new DataParser();
                ParsedData parsedData = dataParser.parser(rawData.Data, rawData.IMEI);
                string jsonString = JsonSerializer.Serialize(parsedData);
                //

                byte[] bytemessage = Encoding.UTF8.GetBytes(jsonString);

                channel.BasicPublish(
                    exchange: "",
                    routingKey: MQ_QUEUE_PARSED,
                    //basicProperties: properties,
                    body: bytemessage);
                //

                channel.BasicAck(ea.DeliveryTag, false);
            };

            channel.BasicConsume(MQ_QUEUE, false, consumer);

        }

        ~Worker()
        {
            channel.Close();
            connection.Close();
            //Console.WriteLine("App closing...");
            _logger.LogInformation("Consumer closing...");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                String s = String.Format("Consumer running at: {0}, received: {1}", DateTimeOffset.Now, counter);
                counter = 0;

                _logger.LogInformation(s);

                await Task.Delay(1000, stoppingToken);
            }
        }

    }
}
