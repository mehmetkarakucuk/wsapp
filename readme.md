﻿
SignalR 
- https://github.com/Microsoft-Web/WebCampTrainingKit/blob/master/HOL/AspNetApiSpa/Source/Ex1-CreatingAnAPI/End/src/GeekQuiz/Startup.cs
- https://github.com/Microsoft-Web/WebCampTrainingKit
- [redis ile genişletme](https://docs.microsoft.com/tr-tr/aspnet/signalr/overview/performance/scaleout-with-redis)
- [kullanıcıları ve grupları yönetme](https://docs.microsoft.com/tr-tr/aspnet/core/signalr/groups?view=aspnetcore-5.0)
- [authn-and-authz](https://github.com/dotnet/AspNetCore.Docs/tree/master/aspnetcore/signalr/authn-and-authz)
- [Authentication and authorization in ASP.NET Core SignalR](https://github.com/dotnet/AspNetCore.Docs/blob/master/aspnetcore/signalr/authn-and-authz.md)
- [Asp.NET MVC 5 ve SignalR İle Chat Uygulaması](https://www.gencayyildiz.com/blog/asp-net-mvc-5-ve-signalr-ile-chat-uygulamasi/)
- [Asp.NET Core – SignalR Serisi #1 – SignalR Nedir? Nasıl Çalışır?](https://www.gencayyildiz.com/blog/asp-net-core-signalr-serisi-1-signalr-nedir-nasil-calisir/)
- [Asp.NET Core – SignalR Yazı Serisi](https://www.gencayyildiz.com/blog/asp-net-core-signalr-yazi-serisi/)
- [RabbitMQ – Fanout Exchange](https://www.gencayyildiz.com/blog/rabbitmq-fanout-exchange/)



Redis - StackExchange.Redis
- https://stackexchange.github.io/StackExchange.Redis/Basics

RabbitMQ
- [Basic Usage](https://www.rabbitmq.com/tutorials/tutorial-one-dotnet.html)


Microsoft.VisualStudio.Web.CodeGeneration.Design package not being found by the dotnet aspnet-codegenerator tool #1393
- https://github.com/dotnet/Scaffolding/issues/1393


Authorize
- [AuthorizeAttribute.Roles Property](https://docs.microsoft.com/tr-tr/dotnet/api/microsoft.aspnetcore.authorization.authorizeattribute.roles?view=aspnetcore-5.0)
- [ASP.NET Core basit yetkilendirme](https://docs.microsoft.com/tr-tr/aspnet/core/security/authorization/simple?view=aspnetcore-5.0)
