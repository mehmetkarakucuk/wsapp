﻿using Microsoft.AspNetCore.SignalR;

namespace wsServer
{
    #region NameUserIdProvider
    public class NameUserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            return connection.User?.Identity?.Name;
        }
    }
    #endregion
}