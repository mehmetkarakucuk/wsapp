﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using StackExchange.Redis;



//ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
//ISubscriber sub = redis.GetSubscriber();
//// Asynchronous handler
//sub.Subscribe("plate").OnMessage(async channelMessage =>
//{
//    await Task.Delay(1000);
//    Console.WriteLine((string)channelMessage.Message);
//});

namespace wsServer
{
    class Program
    {

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
